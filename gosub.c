//
//	Filename:	gosub.c
//
#define	Version		"008"
//
//	Edit date:	2023-01-25
//
//	Facility:	Asterisk
//
//	Abstract:	This AGI 'script' implements the GOSUB widget.
//
//	Environment:	Asterisk
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	2013-09-11	SLE	Create.
//	001	2020-06-05	SLE	Don't syslog() in hangup().
//	002	2020-06-20	SLE	Parse arguments out of DATA.
//					Set PROMPT if passed.
//					Set RETRY-PROMPT if passed.
//	003	2021-02-01	SLE	Add start_time.
//					Add syslog_prefix.
//					Change call_id to global.
//					Change client_id to global.
//	004	2021-03-23	SLE	Treat *IDX as an integer.
//	005	2021-04-19	SLE	Use STEP-PREFIX.
//	006	2022-01-25	SLE	Raise the syslog level for the
//					  duration message.
//	007	2022-12-19	SLE	Allow nesting.
//	008	2023-01-25	SLE	Fix argument parsing.

////////////////////////////////////////|//////////////////////////////////////
// ANSI include files
////////////////////////////////////////|//////////////////////////////////////
#include	<ctype.h>
#include	<signal.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

////////////////////////////////////////|//////////////////////////////////////
// Operating system include files
////////////////////////////////////////|//////////////////////////////////////
#include	<syslog.h>

////////////////////////////////////////|//////////////////////////////////////
// Local include files
////////////////////////////////////////|//////////////////////////////////////
#include	"agi.h"
#include	"all.h"
#include	"getopt.h"
#include	"xxxx.h"

////////////////////////////////////////|//////////////////////////////////////
// Defines
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Macros
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Typedefs
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////
global	int				call_id;
global	int				client_id;
global	char				syslog_prefix[256];
global	int				debug_mode;
global	int				test_mode;
global	int				verbose_mode;

////////////////////////////////////////|//////////////////////////////////////
// Global functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External variables
////////////////////////////////////////|//////////////////////////////////////
extern	char				*optarg;

////////////////////////////////////////|//////////////////////////////////////
// External functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static variables
////////////////////////////////////////|//////////////////////////////////////
static	time_t				start_time;

////////////////////////////////////////|//////////////////////////////////////
// Static functions
////////////////////////////////////////|//////////////////////////////////////
static	void				cleanup
	(
	  void
	);
static	void				hangup
	(
	  void
	);

////////////////////////////////////////|//////////////////////////////////////
// Main function
////////////////////////////////////////|//////////////////////////////////////
global	int				main
	(
	  int				argc
	, char				**argv
	)
	{
	auto	int			argument_idx;
	auto	char			*argument_pointer;
	auto	char			data[2560];
	static	const char		delimiters[] = ", ";
	auto	int			idx;
	static	struct option		long_options[]
		= {
		  {"debug-mode", no_argument, &debug_mode, 1}
		, {"help", no_argument, 0, '?'}
		, {"null", no_argument, 0, 0}
		, {"test-mode", no_argument, &test_mode, 1}
		, {"verbose-mode", no_argument, &verbose_mode, 1}
		, {"version", no_argument, 0, 'v'}
		, {0, 0, 0, 0}
		};
	auto	int			nesting;
	auto	int			optchar;
	auto	char			prompt[1024];
	auto	char			retry_prompt[1024];
	auto	char			step_prefix[256];
	auto	char			temp1[256];
	auto	char			temp2[256];

// save the start time
	start_time = time(0);

// set the syslog ident
	setlogmask(LOG_UPTO(LOG_NOTICE));
	openlog(basename(argv[0]), LOG_PID, LOG_USER);

// trap SIGHUP -- caller hung up
	signal(SIGHUP, (void (*)(int))(long int)hangup);

// read the AGI environment
	agi_read_environment();

// assume failure
	agi_set_status_failure();

// get the CALL-ID
	call_id = agi_get_integer_variable("CALL-ID");

// get the CLIENT-ID
	client_id = agi_get_integer_variable("CLIENT-ID");

// create the syslog prefix
	sprintf(syslog_prefix
		, "%03d-%08d"
		, client_id
		, call_id
		);

// get IDX
	idx = agi_get_integer_variable("IDX");

// get step prefix
	memset(step_prefix, 0, sizeof(step_prefix));
	agi_get_variable("STEP-PREFIX", step_prefix);

// get STEP-PREFIX-DATA
	memset(data, 0, sizeof(data));
	agi_get_variable("%s-DATA", data, step_prefix);

// get STEP-PREFIX-PROMPT
	memset(prompt, 0, sizeof(prompt));
	agi_get_variable("%s-PROMPT", prompt, step_prefix);

// get STEP-PREFIX-RETRY-PROMPT
	memset(retry_prompt, 0, sizeof(retry_prompt));
	agi_get_variable("%s-RETRY-PROMPT", retry_prompt, step_prefix);
		
// process the command line arguments
	while	(-1 != (optchar
			= getopt_long(
				  argc
				, argv
				, ""
				, long_options
				, 0
				)))
		{
		switch	(optchar)
			{
// help
			case '?':
			default:
				printf("Usage:\t%s\\\n", basename(argv[0]));
				puts("\t\t--debug-mode");
				puts("\t\t--help\\");
				puts("\t\t--verbose-mode\\");
				puts("\t\t--version");
				exit(EXIT_SUCCESS);
				break;
// version
			case 'v':
				printf("%s version %s\n"
					, *argv
					, Version
					);
				printf("Compiled on %s at %s\n"
					, __DATE__
					, __TIME__
					);
				exit(EXIT_SUCCESS);
				break;

// something automagically handled by getopt_long()
			case 0:
				break;
			}
		}

// set verbose mode
	if	(0 != verbose_mode)
		{
		setlogmask(LOG_UPTO(LOG_INFO));
		}

// set debug mode
	if	(0 != debug_mode)
		{
		setlogmask(LOG_UPTO(LOG_DEBUG));
		}

// say who we are
	syslog(LOG_INFO
		, "%s Starting, version %s"
		, syslog_prefix
		, Version
		);

// make sure we clean up after ourselves
	atexit(&cleanup);

// set PROMPT
	if	(0 != *prompt)
		{
		agi_set_variable("PROMPT", prompt);
		}

// set RETRY-PROMPT
	if	(0 != *retry_prompt)
		{
		agi_set_variable("RETRY-PROMPT", retry_prompt);
		}

// nesting
// (looking for the first unused RETURN)
	for	(nesting = 0; ; nesting++)
		{
		memset(temp1, 0, sizeof(temp1));
		agi_get_variable("RETURN_%d", temp1, nesting);
		if	(0 == *temp1)
			{
			break;
			}
		}

// set RETURN_x
	sprintf(temp1, "%03d", idx);
	sprintf(temp2, "RETURN_%d", nesting);
	agi_set_variable(temp2, temp1);
	syslog(LOG_ERR
		, "%s RETURN_%d = %d"
		, syslog_prefix
		, nesting
		, idx
		);

// parse the target
	argument_pointer = strtok(data, delimiters);

// were we passed a LABEL?
	if	(0 != isalpha(*argument_pointer))
		{
		memset(temp1, 0, sizeof(temp1));
		agi_get_variable("LABEL-%s", temp1, argument_pointer);
		}

// decrement the IDX because it will be incremented in the dialplan
	idx = atoi(temp1);
	--idx;

// set the IDX channel variable
//	sprintf(temp1, "%03d", idx);
//	agi_set_variable("IDX", temp1);
	agi_set_integer_variable("IDX", idx);

// parse the arguments and create channel variables
	argument_idx = 0;
//	argument_pointer = strtok(0, delimiters);
	while	((argument_pointer = strtok(0, delimiters)))
		{
		auto	char	argument_name[256];
		auto	char	*equal_sign_pointer;
		if	(0 != (equal_sign_pointer = strchr(argument_pointer, '=')))
			{
			*equal_sign_pointer++ = 0;
			agi_set_variable(argument_pointer, equal_sign_pointer);
			}
		else
			{
			sprintf(argument_name, "ARGUMENT-%d", argument_idx++);
			agi_set_variable(argument_name, argument_pointer);
			}
		}

// return our status
	agi_set_status_success();

// log the duration
	syslog(LOG_NOTICE
		, "%s %d seconds"
		, syslog_prefix
		, (int)(time(0) - start_time)
		);

// function exit
	return(EXIT_SUCCESS);		// return function status
	}				// end of main()

////////////////////////////////////////|///////////////////////////////|//////
// cleanup()
//
////////////////////////////////////////|///////////////////////////////|//////
static	void				cleanup
	(
	  void
	)
	{
	exit(EXIT_SUCCESS);
	}

////////////////////////////////////////|///////////////////////////////|//////
// hangup()
//
// Asterisk delivers a SIGHUP when the caller hangs up. Since we have
// already registered a process termination function, all we have to
// do is exit.
////////////////////////////////////////|///////////////////////////////|//////
static	void				hangup
	(
	  void
	)
	{
//	syslog(LOG_ERR, "hangup detected");
	exit(EXIT_SUCCESS);
	}

// (end of gosub.c)
