# Portfolio

A portfolio of code I feel is representative of my work.

## auth-card.c

(Requested by a member of https://community.asterisk.org/ so, included here.)

A very, very old AGI that demonstrates executing a task while playing
a prompt in a backgroud thread.

The use case was to 'hide' the credit card authorization delay for a
better caller experience.

Processing a credit card authorization can take a couple of
seconds. To ‘hide’ the dead air, I play a prompt like ‘Get ready for a
wild experience’ as a background thread and execute my DB and API
calls for the auth. When the auth completes, I join the background
thread and the caller has a ‘seamless’ experience.

## branch.c

An AGI (Asterisk Gateway Interface) that implements a verb in a small
database driven scripting language.

It asks a question and branches to different locations in their
dialplan based on DTMF entered by the caller.

This is an old program that has suffered substantial featurism but
has held up well.

### Example

Here is a database entry that prompts the caller to confirm a spoken
account number and allows the caller to accept or re-enter.

```sql
-- confirm account number
	set @IDX := @IDX + 1;
	insert into steps set
		  created		= current_timestamp
		, client_id		= @CLIENT_ID
		, idx			= @IDX
		, active		= @ACTIVE
		, data			= 'NEXT'			-- 1 correct
					 ',ENTER-ACCOUNT-NUMBER'	-- 2 re-enter
		, prompt		= 'xxxx/621/621-33-prompt'
					 '&v:ACCOUNT-NUMBER-NAME'
					 '&xxxx/621/616-10-prompt'
		, recipe		= @RECIPE
		, type			= 'BRANCH'
		;
```

Other verbs include: account-number, agent-id, branch, datetime, dtmf,
finish-call, goodbye, gosub, goto, language, length, math, prompt,
return, set, simple-set, string, substring, variable, verbose, voice

## gosub.c

An AGI (Asterisk Gateway Interface) that implements a verb in a small
database driven scripting language.

It allows a user to execute a subroutine, passing prompts and arguments
if specified. It defines the return address that is used by the return
verb.

### Example

Here is a database entry that executes a subroutine to setup prompts
and variables for a specified branch number.


```sql
-- lookup branch variables
        set @IDX := @IDX + 1;
        insert into steps set
                  created               = current_timestamp
                , client_id             = @CLIENT_ID
	        , data                  = 'SETUP-PROMPTS-AND-VARIABLES,BRANCH-NUMBER=1'
                , idx                   = @IDX
                , recipe                = @RECIPE
                , type                  = 'GOSUB'
                ;
```

## manage-partitions.pl

MySQL 5.7 introduced partitioned tables. The most useful partitioning
strategy (to me) is date ranges. This improves performance (a
subject of some debate) and facilitates database administration.

MySQL does not include a utility to manage partitions, relying on
commands entered into the MySQL shell.

manage-partitions.pl is that missing utility. You can specify the
number of days / months / years you want to partition your data across
and it will create the the partitions and maintain those
specifications as you run the script in the future (cron recommended),
creating partitions and rolling up partitions as needed.

One feature of note is that it doesn't actually execute the
commands. Rather, it outputs the commands to stdout so if you approve,
all you have to do is pipe it through Bash for execution, potentially
saving you from accidentially reorganizing your multi-terrabyte tables
during production :)

### Example

```bash
./manage-partitions.pl --example

	./manage-partitions.pl\
		--analyze\
		--database=xxxx\
		--days=60\
		--host=localhost\
		--months=2\
		--password=${password}\
		--stop-on-error\
		--table=Traffic\
		--user=${user}\
		--verbose\
		${end_of_list}

Add a 'minval' partition if needed.
Add a 'maxval' partition if needed.

Create partitions for the previous 60 days if needed.
Create a partition for tomorrow if needed.

Roll the day partitions up to the month partition for any whole month
older than 2 months.
(Note that only 1 month will be rolled up per run.)
Roll the month partitions up to the year partition for any month older
than 14 months.
(Note that only 1 year will be rolled up per run.)

Analyze the partitions we created.

Show extra information about what is happening.
Stop if any errors are detected.

If all that looks reasonable, run it again and pipe it through Bash.
```

## schedule-target-calls.php

Query a database and create Asterisk call files.

The script gets run through a preprocessor to replace tokens
(for example @PRODUCTION_PASSWORD@) with literal values.

## upload-to-playback.sh

A Bash script that uploads files to a web server.

The script gets run through a preprocessor to replace tokens
(for example @PRODUCTION_PASSWORD@) with literal values.

## iptables-log-filter.php

A script that reads iptables kernel log messages and creates iptable
rules.

## extensions.ael.pre

An Asterisk dialplan written in AEL.

The dialplan gets run through a preprocessor to replace tokens
(for example @PRODUCTION_PASSWORD@) with literal values.

This dialplan implements a small database driven scripting language.

## updating-production-data.pdf

A document explaining how to (somewhat) safely update production data.
