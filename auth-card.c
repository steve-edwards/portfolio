// watch out for trying to execute agi cruft while the prompt thread
// is running.

// process_request should not talk to rap if the response is going to
// be overridded.

//
//	Filename:	auth-card.c
//
#define	Version		"022"
//
//	Edit date:	2013-08-20
//
//	Facility:	Asterisk
//
//	Abstract:	This AGI 'script' implements the auth-card
//			application.
//
//	Environment:	Asterisk
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	07-Mar-2005	SLE	Create.
//	001	01-Apr-2005	SLE	Add forced_response.
//					Add forced_status;
//	002	13-Jun-2005	SLE	Add customer_sequence_number.
//	003	02-Aug-2005	SLE	Get AUTHORIZATION-AMOUNT from
//					  channel variables instead of
//					  database.
//					Get IP-CODE from channel
//					  variables instead of
//					  database.
//	004	18-Mar-2006	SLE	Add transaction_date to
//					  process_request().
//	005	11-May-2006	SLE	Use DATABASE-SERVER channel
//					  variable.
//	006	20-Jun-2006	SLE	Add block-card feature.
//	007	2007-09-18	SLE	Add NPA
//	008	2007-10-04	SLE	Don't force DEBUG log level.
//	009	2008-11-15	SLE	Add database channel variables.
//	010	2008-12-18	SLE	Add CUSTOMER.
//	011	2008-12-29	SLE	Add --null.
//	012	2009-01-05	SLE	Log DATABASE-PRODUCTION-SERVER
//					  undefined errors.
//	013	2009-01-07	SLE	Add --check-mode.
//	014	2009-02-09	SLE	Log when response is
//					  overridden.
//					Log when status is
//					  overridden.
//	015	2009-02-11	SLE	Reorder code so we don't try
//					  to exec_verbose() while the
//					  prompt thread is running.
//	016	2010-11-05	SLE	Use basename() in openlog().
//					Use basename() in usage.
//	017	2010-11-19	SLE	Add --silent.
//	018	2011-12-12	SLE	Handle multiple cards rows.
//	019	2011-12-19	SLE	Join prompt thread if debug
//					  mode is set.
//					Join prompt thread before
//					  overridden message is
//					  output to Asterisk.
//	020	2012-11-29	SLE	Add CVV2.
//					Add STREET.
//					Add ZIP.
//	021	2013-07-17	SLE	Add host to process_request().
//					Add port to process_request().
//					Add timeout to process_request().
//					Code cleanups.
//	022	2013-08-20	SLE	Log elapsed time.

////////////////////////////////////////|//////////////////////////////////////
// ANSI include files
////////////////////////////////////////|//////////////////////////////////////
#include	<ctype.h>
#include	<errno.h>
#include	<pthread.h>
#include	<stdarg.h>
#include	<stddef.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

////////////////////////////////////////|//////////////////////////////////////
// Operating system include files
////////////////////////////////////////|//////////////////////////////////////
#include	<syslog.h>

////////////////////////////////////////|//////////////////////////////////////
// Local include files
////////////////////////////////////////|//////////////////////////////////////
#include	"agi.h"
#include	"all.h"
#include	"getopt.h"
#include	"mysql.h"
#include	"rap.h"
#include	"utility.h"

////////////////////////////////////////|//////////////////////////////////////
// Defines
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Macros
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Typedefs
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External variables
////////////////////////////////////////|//////////////////////////////////////
extern	char				*optarg;

////////////////////////////////////////|//////////////////////////////////////
// External functions
////////////////////////////////////////|//////////////////////////////////////
extern	int				is_valid_card
	(
	  const char			*card_number_pointer
	, const char			*expiration_date_pointer
	);

////////////////////////////////////////|//////////////////////////////////////
// Static constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static variables
////////////////////////////////////////|//////////////////////////////////////
static	MYSQL				mysql;
static	MYSQL_ROW			mysql_row;
static	char				card_number[256];
static	char				customer[256];
static	char				cvv2[256];
static	char				dnis[256];
static	int				authorization_amount;
static	int				check_mode;
static	int				debug_mode;
static	int				silent_mode;
static	time_t				start_time;
static	char				street[256];
static	int				verbose_mode;
static	char				zip[256];

////////////////////////////////////////|//////////////////////////////////////
// Static functions
////////////////////////////////////////|//////////////////////////////////////
static	void				cleanup
	(
	  void
	);
static	int				play_prompt
	(
	  int				dummy
	);

////////////////////////////////////////|//////////////////////////////////////
// Main function
////////////////////////////////////////|//////////////////////////////////////
global	int				main
	(
	  int				argc
	, char				**argv
	)
	{
	auto	char			database_database[256];
	auto	char			database_password[256];
	auto	char			database_production_server[256];
	auto	char			database_username[256];
	auto	char			ani[256];
	auto	char			globalid[256];
	auto	char			expiration_date[256];
	auto	int			forced_response;
	auto	int			forced_status;
	auto	char			ip_code[256];
	static	struct option		long_options[]
		= {
		  {"check-mode", no_argument, &check_mode, 1}
		, {"debug-mode", no_argument, &debug_mode, 1}
		, {"force-response", required_argument, 0, 'r'}
		, {"force-status", required_argument, 0, 's'}
		, {"help", no_argument, 0, '?'}
		, {"null", no_argument, 0, 0}
		, {"silent-mode", no_argument, &silent_mode, 1}
		, {"verbose-mode", no_argument, &verbose_mode, 1}
		, {"version", no_argument, 0, 'v'}
		, {0, 0, 0, 0}
		};
	auto	char			npa[256];
	auto	int			optchar;
	auto	RAP_PACKET		rap_packet;
	auto	int			rap_response;
	auto	char			host[256];
	auto	int			port;
	auto	int			rap_status;
	auto	MYSQL_RES		*result_pointer;
	auto	int			status;
	auto	char			temp[256];
	auto	pthread_t		thread_id;
	auto	int			timeout;
	auto	int			transaction_type;

// save the start time
	start_time = time(0);

// set the syslog ident
	setlogmask(LOG_UPTO(LOG_NOTICE));
	openlog(basename(argv[0]), LOG_PID, LOG_USER);

// say who we are
	syslog(LOG_ERR
		, "Starting, version %s"
		, Version
		);

// process the command line arguments
	forced_response
		= forced_status
			= -1;
	rap_response
		= rap_status
			= 0;
	while	(-1 != (optchar
			= getopt_long(
				  argc
				, argv
				, ""
				, long_options
				, 0
				)))
		{
		switch	(optchar)
			{
// help
			case '?':
			default:
				printf("Usage:\t%s\\\n", basename(argv[0]));
				puts("\t\t--debug-mode\\");
				puts("\t\t--help\\");
				puts("\t\t--null\\");
				puts("\t\t--verbose-mode\\");
				puts("\t\t--version");
				exit(AGI_SUCCESS);
				break;
// forced-response
			case 'r':
				forced_response = atoi(optarg);
				break;
// forced-status
			case 's':
				forced_status = atoi(optarg);
				break;
// version
			case 'v':
				printf("%s version %s\n"
					, *argv
					, Version
					);
				printf("Compiled on %s at %s\n"
					, __DATE__
					, __TIME__
					);
				exit(AGI_SUCCESS);
				break;

// options automagically handled by getopt_long()
// debug-mode
// verbose-mode
			case 0:
				break;
			}
		}

// set verbose mode
	if	(0 != verbose_mode)
		{
		setlogmask(LOG_UPTO(LOG_INFO));
		}

// set debug mode
	if	(0 != debug_mode)
		{
		setlogmask(LOG_UPTO(LOG_DEBUG));
		}

// read the AGI environment
	read_agi_environment();

// assume failure
	exec_agi("SET PRIORITY %d"
		, 101
		);

// get the ANI
	memset(ani, 0, sizeof(ani));
	get_variable("ANI", ani);

// get the AUTHORIZATION-AMOUNT
	memset(temp, 0, sizeof(temp));
	get_variable("AUTHORIZATION-AMOUNT", temp);
	authorization_amount = atoi(temp);

// get the CARD-NUMBER
	memset(card_number, 0, sizeof(card_number));
	get_variable("CARD-NUMBER", card_number);

// get the CUSTOMER
	memset(customer, 0, sizeof(customer));
	get_variable("CUSTOMER", customer);

// get the CVV2
	memset(cvv2, 0, sizeof(cvv2));
	get_variable("CVV2", cvv2);

// get the database credentials
	memset(database_production_server, 0, sizeof(database_production_server));
	get_variable("DATABASE-PRODUCTION-SERVER", database_production_server);
	if	(0 == *database_production_server)
		{
		syslog(LOG_ERR
			, "DATABASE-PRODUCTION-SERVER is undefined."
			);
		exec_verbose("DATABASE-PRODUCTION-SERVER is undefined.");
		exit(EXIT_FAILURE);
		}
	memset(database_database, 0, sizeof(database_database));
	get_variable("DATABASE-DATABASE", database_database);
	memset(database_password, 0, sizeof(database_password));
	get_variable("DATABASE-PASSWORD", database_password);
	memset(database_username, 0, sizeof(database_username));
	get_variable("DATABASE-USERNAME", database_username);

// get the DNIS
	memset(dnis, 0, sizeof(dnis));
	get_variable("DNIS", dnis);

// get the EXPIRATION-DATE
	memset(expiration_date, 0, sizeof(expiration_date));
	get_variable("EXPIRATION-DATE", expiration_date);

// get the GLOBALID
	memset(globalid, 0, sizeof(globalid));
	get_variable("GLOBALID", globalid);

// get the IP-CODE
	memset(ip_code, 0, sizeof(ip_code));
	get_variable("IP-CODE", ip_code);

// get the NPA
	memset(npa, 0, sizeof(npa));
	get_variable("NPA", npa);

// get the TPS variables
	memset(host, 0, sizeof(host));
	get_variable("TPS-SERVER-HOST", host);
	memset(temp, 0, sizeof(temp));
	get_variable("TPS-SERVER-PORT", temp);
	port = atoi(temp);
	memset(temp, 0, sizeof(temp));
	get_variable("TPS-SERVER-TIMEOUT", temp);
	timeout = atoi(temp);

// get the STREET
	memset(street, 0, sizeof(street));
	get_variable("STREET", street);

// get the ZIP
	memset(zip, 0, sizeof(zip));
	get_variable("ZIP", zip);

// create the prompt thread if needed
	thread_id = 0;
	if	(('9' != *npa)
	&&	 (0 == check_mode)
	&&	 (0 == silent_mode))
		{
		status = pthread_create(&thread_id
					// thread id
			, 0		// attributes
			, (void *)&play_prompt
					// start routine
			, 0		// argument
			);
		}

// if debugging, wait for the prompt thread to complete to make life
// easier.
	if	(0 != debug_mode)
		{
		if	(0 != thread_id)
			{
			pthread_join(thread_id, 0);
			}
		}

// make sure we clean up after ourselves
	atexit(&cleanup);

// a little sanity checking on the card number and exipration date
	if	(0 == check_mode)
		{
		status = is_valid_card(card_number, expiration_date);
		if	(EXIT_SUCCESS != status)
			{
			auto	char		temp[256];
			if	(0 != thread_id)
				{
				pthread_join(thread_id, 0);
				}
			sprintf(temp
				, "Invalid card = \"%s,\" expiration date = \"%s\""
				, card_number
				, expiration_date
				);
			syslog(LOG_DEBUG
				, "%s"
				, temp
				);
			exec_verbose(temp);
			exit(AGI_FAILURE);
			}
		}

// connect to the database
	mysql_init(&mysql);
	if	(!mysql_real_connect(
			  &mysql	// context
			, database_production_server
					// host
			, database_username
					// username
			, database_password
					// password
			, database_database
					// database
			, 0		// port
			, 0		// unix_socket
			, 0		// client_flag
			))
		{
		if	(0 != thread_id)
			{
			pthread_join(thread_id, 0);
			}
		syslog(LOG_ERR
			, "%m, mysql_error = \"%s\""
			, mysql_error(&mysql)
			);
		exec_verbose(
			  "Failed to connect to the database"
				", mysql_error = \"%s\""
			, mysql_error(&mysql)
			);
		exit(AGI_FAILURE);
		}

// check our card database
	exec_sql(&mysql
		, &result_pointer
		, "select reason from cards"
		  " where card_number = '%s'"
		  " and expires > now()"
		, card_number
		);
	if	(0 < mysql_num_rows(result_pointer))
		{
		if	(0 != thread_id)
			{
			pthread_join(thread_id, 0);
			}
		if	(0 == (mysql_row = mysql_fetch_row(result_pointer)))
			{
			syslog(LOG_ERR
				, "?Weird -- no columns returned?"
				);
			exit(AGI_FAILURE);
			}
		forced_response = atoi(mysql_row[0]);
		sprintf(temp
			, "%s response will be overridden with %d"
			, card_number
			, forced_response
			);
		syslog(LOG_ERR, "%s", temp);
		exec_verbose(temp);
		}

// set the transaction type
	if	(0 == check_mode)
		{
		transaction_type = OPEN;
		}
	else
		{
		transaction_type = CHECK_OPEN;
		}

// process the authorization request
	status = process_request(
		  &mysql		// database pointer
		, host			// address or name
		, port			// port number
		, timeout		// timeout
		, &rap_packet		// rap packet
		, ani			// ani
		, card_number		// card number
		, authorization_amount	// charge
		, globalid		// customer sequence number
		, cvv2			// cvv2
		, dnis			// dnis
		, expiration_date	// expiration date
		, ip_code		// ip code
		, 0			// second
		, transaction_type	// transaction type
		, 0			// transaction date
		, &rap_status		// rap status pointer
		, &rap_response		// rap response pointer
		, forced_status		// forced rap status
		, forced_response	// forced rap response
		, street		// street number
		, zip			// ZIP code
		);
syslog(LOG_ERR
, "%d %d %d %d"
, rap_status
, rap_response
, forced_status
, forced_response
);

// close the database connection
	mysql_close(&mysql);

// wait for the prompt thread
	if	(0 != thread_id)
		{
		pthread_join(thread_id, 0);
		}

// processing failed
	if	(EXIT_SUCCESS != status)
		{
		syslog(LOG_ERR
			, "Failed to process the request"
			);
		exit(AGI_FAILURE);
		}

// check for forced status
	if	(-1 != forced_status)
		{
		sprintf(temp
			, "%s overriding status %d with %d"
			, card_number
			, rap_status
			, forced_status
			);
		syslog(LOG_ERR, "%s", temp);
		exec_verbose(temp);
		rap_status = forced_status;
		}

// set the exit priority
	exec_agi("SET PRIORITY %d"
		, rap_response
		);

// log the elapsed time
	syslog(LOG_ERR
		, "%s %d seconds"
		, globalid
		, (int)(time(0) - start_time)
		);

// function exit
	return(AGI_SUCCESS);		// return function status
	}				// end of main()

////////////////////////////////////////|///////////////////////////////|/////
// play_prompt()
//
// This function plays the prompt.
////////////////////////////////////////|///////////////////////////////|/////
static	int				play_prompt
	(
	  int				dummy
	)
	{

// say what we are going to do
	exec_agi("STREAM FILE %s/menu/m1313 ''", customer);

	return(EXIT_SUCCESS);
	}

////////////////////////////////////////|///////////////////////////////|//////
// cleanup()
//
////////////////////////////////////////|///////////////////////////////|//////
static	void				cleanup
	(
	  void
	)
	{
	mysql_close(&mysql);
	exit(AGI_SUCCESS);
	}

// (end of auth-card.c)
