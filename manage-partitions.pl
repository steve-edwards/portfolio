#!/usr/bin/perl

#	Filename:	manage-partitions.pl
#
#	Version		004
#
#	Edit date:	2019-10-21
#
#	Facility:	MySQL 'missing' utilities
#
#	Abstract:	Manage partitions
#
#	Environment:	Linux/MySQL 8.0
#
#	Author:		Steven L. Edwards
#
#	Modified by
#
#	000	2019-10-21	SLE	Create.
#	001	2019-10-23	SLE	Update description.
#	002	2019-10-27	SLE	Change roll up partition names.
#	003	2019-11-21	SLE	Add --example.
#					Change partition names from
#					  using dashes to underscores. 
#	004	2020-09-21	SLE	Change default table.

# perldb arguments
# --test

# ttd
# roll up month to yyyy-mm
# roll up year to yyyy

# bugs
# adding a maxval partition reveals other errors

# how to create the manage-partitions user:
#	select host,user from mysql.user order by 1,2;
#	drop user if exists 'manage-partitions'@192.168.0.46;
#	drop user if exists 'manage-partitions'@'%';
#	create user 'manage-partitions'@192.168.0.46 identified with mysql_native_password by 'foo';
#	create user 'manage-partitions'@192.168.0.46 identified with caching_sha2_password by 'foo';
#	grant alter on xxxx.* to 'manage-partitions'@192.168.0.46;
#	grant insert on xxxx.* to 'manage-partitions'@192.168.0.46;
#	grant select on xxxx.* to 'manage-partitions'@192.168.0.46;
#	(not really needed for production but convenient for testing)
#	grant create on xxxx.* to 'manage-partitions'@192.168.0.46;
#	grant drop on xxxx.* to 'manage-partitions'@192.168.0.46;

########|###############################################################
# declare modules
########|###############################################################
use	5.010;
use	strict;
use	warnings;

use	DBI;
use	Data::Dumper;
use	Getopt::Long;
use	POSIX qw(strftime);
use	POSIX;

########|###############################################################
# define constants
########|###############################################################
use	constant			MAXVALUE=> 3652424;	# to_days('9999-12-31')
use	constant			bs	=> '\\';	# backslash
use	constant			bt	=> '`';		# backtick
use	constant			dq	=> '"';		# double-quote
use	constant			nl	=> "\n";	# newline
use	constant			nlx2	=> "\n" x 2;	# 2 newlines
use	constant			sq	=> "'";		# single quote
use	constant			tab	=> "\t";	# tab
use	constant			tabx2	=> "\t" x 2;	# 2 tabs
use	constant			tabx3	=> "\t" x 3;	# 3 tabs
use	constant			tabx4	=> "\t" x 4;	# 4 tabs
use	constant			tabx5	=> "\t" x 5;	# 5 tabs

########|###############################################################
# define variables
########|###############################################################
my $command;
my $dbh;
my $debug;
my $disable_binlog;
my $mysql;
my $sql;
my $sth;
my @analyze_partitions;
my @current_partitions;
my @options = @ARGV;
my @output;
my @row;

# Wrap descriptions at 32 characters and indent subsequent lines 5 tabs.
########|###############################################################
sub					examples
########|###############################################################
	{
        print nl
		. tab . './manage-partitions.pl'		. bs . nl
		. tabx2 . '--analyze'				. bs . nl
		. tabx2 . '--database=xxxx'			. bs . nl
		. tabx2 . '--days=60'				. bs . nl
		. tabx2 . '--host=localhost'			. bs . nl
		. tabx2 . '--months=2'				. bs . nl
		. tabx2 . '--password=${password}'		. bs . nl
		. tabx2 . '--stop-on-error'			. bs . nl
		. tabx2 . '--table=Traffic'			. bs . nl
		. tabx2 . '--user=${user}'			. bs . nl
		. tabx2 . '--verbose'				. bs . nl
		. tabx2 . '${end_of_list}'			. nl
		. nl
		. 'Add a ' . sq . 'minval' . sq . ' partition if needed.' . nl
		. 'Add a ' . sq . 'maxval' . sq . ' partition if needed.' . nl
		. nl
		. 'Create partitions for the previous 60 days if needed.' . nl
		. 'Create a partition for tomorrow if needed.' . nl
		. nl
		. 'Roll the day partitions up to the month partition for any whole month' . nl
		. 'older than 2 months.' . nl
		. '(Note that only 1 month will be rolled up per run.)' . nl
		. 'Roll the month partitions up to the year partition for any month older' . nl
		. 'than 14 months.' . nl
		. '(Note that only 1 year will be rolled up per run.)' . nl
		. nl
		. 'Analyze the partitions we created.' . nl
		. nl
		. 'Show extra information about what is happening.' . nl
		. 'Stop if any errors are detected.' . nl
		. nl
		. 'If all that looks reasonable, run it again and pipe it through bash.' . nl
		;

	return;
	}

########|###############################################################
sub					full_help
########|###############################################################
	{
        print nl
		. tab . '--debug'				. tabx4 . 'Make debugging easier.'		. nl
		. tab . '--disable-binlog'			. tabx2 . 'Disable bin log for each session.'	. nl
		. tab . '--examples'				. tabx3	. 'Show example commands.'		. nl
		. tab . '--stop-on-error'			. tabx3 . 'Add \'set -e\' to the script'	. nl
								. tabx5 . 'output.'				. nl
								. tabx5 . '(Highly recommended.)'		. nl
		. tab . '--verbose'				. tabx3 . 'Include statements to log'		. nl
								. tabx5 . 'progress in the script output.'	. nl
								. tabx5 . '(Highly recommended.)'		. nl
		;
	}

########|###############################################################
sub					future_help
########|###############################################################
	{
	print 'Manage partitions'										. nlx2
		. tab . '--help'				. tabx4 . 'Basic usage.'			. nl
		. tab . '--full-help'				. tabx3 . 'More detailed help.'			. nl
		. tab . '--future-help'				. tabx3 . 'Unimplemented features...'		. nl
		;
	}

########|###############################################################
sub					help
########|###############################################################
	{
	print 'Manage partitions'										. nlx2
		. tab . '--help'				. tabx4	. 'Basic usage.'			. nl
		. tab . '--full-help'				. tabx3	. 'More detailed help.'			. nl
		. tab . '--future-help'				. tabx3	. 'Unimplemented features...'		. nl
		. nl
		. tab . '--database=<database-name>'		. tab	. 'Set the database.'			. nl
		. tab . '--host=<host-name>'			. tabx2 . 'Set the host.'			. nl
		. tab . '--login-path=<login-path>'		. tab	. 'Set the login path.'			. nl
		. tab . '--password=<password>'			. tabx2	. 'Set the password.'			. nl
		. tab . '--table=<table-name>'			. tabx2	. 'Set the table.'			. nl
		. tab . '--user=<user-name>'			. tabx2 . 'Set the user.'			. nl
		. nl
		. tab . '--days=<n>'				. tabx3 . 'How many days before rollup.'	. nl
		. tab . '--months=<n>'				. tabx3 . 'How many months before rollup.'	. nl
		;
	}

########|###############################################################
sub					alter_table
#
# [2] name (2019-06-02)
########|###############################################################
	{
	my ($table, $old_partitions, $new_partitions) = @_;
	push @analyze_partitions, @$new_partitions;
	say 'old partitions' . nl . Dumper(@$old_partitions) if $debug;
	say 'new partitions' . nl . Dumper(@$new_partitions) if $debug;

# old partitions
	print $mysql
	    	. tabx2
		. '--execute='
		. sq
		. bs
		. nl
		. tabx3
		. 'alter table '
		. bt . $table . bt
		. bs
		. nl
		. tabx4
		. 'reorganize partition'
		. bs
		. nl
		. tabx5
		. '  '
		. bt . @$old_partitions[0]->[2] . bt
		. bs
		. nl
		;
	for	(my $idx = 1; $idx < scalar(@{$old_partitions}); $idx++)
		{
		print tabx5
			. ', '
			. bt . @$old_partitions[$idx]->[2] . bt
			. bs
		    	. nl
			;
		}

# new partitions
#$DB::single = 1;
	print tabx4
		. 'into'
		. tab
		. '('
		. bs
		. nl
		. tabx5
		. '  partition '
		. bt . partition_name(@$new_partitions[0]) . bt
		. ' values less than ('
		. partition_value(@$new_partitions[0])
		. ')'
		. bs
		. nl
		;
	for	(my $idx = 1; $idx < scalar(@{$new_partitions}); $idx++)
		{
		print tabx5
			. ', partition '
			. bt . partition_name(@$new_partitions[$idx]) . bt
			. ' values less than ('
			. partition_value(@$new_partitions[$idx])
			. ')'
			. bs
			. nl
			;
		}
	print tabx5
		. ')'
		. bs
		. nl
		. tabx4
		. ';'
		. sq
		. bs
		. nl
		. tabx2				
		. '2>&1'
		. ' | awk '
		. sq
		. '$13 != "insecure."'
		. sq
		. nl
		;

	}

########|###############################################################
sub					create_partitions
#
# [1] value as days (737578)
# [2] name (2019-06-02)
########|###############################################################
	{
	my ($table, $days_before_rollup) = @_;
	my $idx;
	my $value;

# get all the day partitions we want (including tomorrow)
	my @create_partitions;
	$sql = 'select'
		. '  date_sub(current_date, interval ? day) as value_as_date'
		. ', to_days(date_sub(current_date, interval ? day)) as value_as_days'
		. ', date_format(date_sub(current_date, interval ? day),'
			. sq . '%Y_%m_%d' . sq . ') as name'
		. ';'
		;
	$sth = $dbh->prepare($sql);
$DB::single = 1;
	for	(my $idx = $days_before_rollup; $idx >= 0; $idx--)
		{
		$sth->execute($idx - 2, $idx - 2, $idx - 1);
		my @row = $sth->fetchrow_array();
		push @create_partitions, \@row;
		}

# get the start of the day partitions we need to create
	for	(my $idx1 = 0; $idx1 <= $#create_partitions; $idx1++)
		{
		my $create_partitions_value = $create_partitions[$idx1][1];
		for	(my $idx2 = 0; $idx2 <= $#current_partitions; $idx2++)
			{
			if	($create_partitions_value eq $current_partitions[$idx2][1])
				{
				$create_partitions[$idx1] = '';
				last;
				}
			}
		if	($create_partitions[$idx1] ne '')
			{
			last;
			}
		}
	@create_partitions = grep {$_ ne ''} @create_partitions;

# get the end of the day partitions we need to create
	for	(my $idx1 = $#create_partitions; $idx1 >= 0; $idx1--)
		{
		my $create_partitions_value = $create_partitions[$idx1][1];
		for	(my $idx2 = $#current_partitions; $idx2 >= 0; $idx2--)
			{
			if	($create_partitions_value eq $current_partitions[$idx2][1])
				{
				$create_partitions[$idx1] = '';
				last;
				}
			}
		if	($create_partitions[$idx1] ne '')
			{
			last;
			}
		}
	@create_partitions = grep {$_ ne ''} @create_partitions;

# any work to be done?
	if	(!@create_partitions)
		{
		print tab
			. '${DATE} +'
			. sq
			. '%F %T No more partitions need to be created.'
			. sq
			. nl
			;
		return;
		}

# add in the next higher partition
#$DB::single = 1;
	$value = $create_partitions[$#create_partitions]->[1];
	for	($idx = 0; $idx <= $#current_partitions; $idx++)
		{
		if	($current_partitions[$idx][1] >= $value)
			{
			last;
			}
		}
	push @create_partitions, $current_partitions[$idx];

# get the end of the current day partitions we need to reorganize
	my @reorganize_partitions = @current_partitions;
	my @temp_partition;
	my $max_create_partitions_value = $create_partitions[$#create_partitions][1];
	for	(my $idx1 = $#reorganize_partitions; $idx1 >= 0; $idx1--)
		{
		if	($reorganize_partitions[$idx1][1] > $max_create_partitions_value)
			{
			@temp_partition = $reorganize_partitions[$idx1];
			$reorganize_partitions[$idx1] = '';
			}
		else
			{
			last;
			}
		}
	@reorganize_partitions = grep {$_ ne ''} @reorganize_partitions;
#	push @reorganize_partitions, @temp_partition;

# get the start of the current day partitions we need to reorganize
	my $min_create_partitions_value = $create_partitions[0][1];
	my $idx1;
	for	($idx1 = 0; $idx1 <= $#reorganize_partitions; $idx1++)
		{
		if	($reorganize_partitions[$idx1][1] >= $min_create_partitions_value)
			{
			last;
			}	
		@temp_partition = $reorganize_partitions[$idx1];
		$reorganize_partitions[$idx1] = '';
		}
	@reorganize_partitions = grep {$_ ne ''} @reorganize_partitions;
#	unshift @reorganize_partitions, @temp_partition;
#$DB::single = 1;

# show the partitions to be reorganized
	if	($debug)
		{
		print '# Reorganizing partitions:' . nl;
		foreach	(@reorganize_partitions)
			{
			print '# ' . tab . @$_[2] . nl;
			}
		print nl;
		}

# show the partitions to be created
	if	($debug)
		{
		print '# Creating partitions:' . nl;
		foreach	(@create_partitions)
			{
			print '# ' . tab . @$_[2] . nl;
			}
		print nl;
		}

# reorganize the partitions as needed, creating as needed
	print tab
		. '${DATE} +'
		. sq
		. '%F %T Create day partitions, including 1 for tomorrow'
		. sq
		. nl
		;
	alter_table($table, \@reorganize_partitions, \@create_partitions);
	print nl;

	return;
	}

########|###############################################################
sub					partition_name
#
# [0] value as date (2019-06-03)
# [1] value as days (737578)
########|###############################################################
	{
	my ($partition) = @_;
#$DB::single = 1;

	if	($partition->[1] == 0)
		{
		return('0000');
		}
	elsif	($partition->[1] == MAXVALUE)
		{
		return('9999');
		}
	$sql = 'select'
		. ' date_format(date_sub(?, interval 1 day), '
		. sq
		. '%Y-%m-%d'
		. sq
		. ')'
		. ', date_format(last_day(date_sub(?, interval 1 day)), '
		. sq
		. '%Y-%m-%d'
		. sq
		. ')'
		. ';'
		;
	$sth = $dbh->prepare($sql);
	$sth->execute($partition->[0], $partition->[0]);
	my @row = $sth->fetchrow_array();
	if	($row[0] eq $row[1])
		{
		$row[0] = substr($row[0], 0, 7);
		}
	if	($row[0] eq (substr($row[0], 0, 4) . '-12'))
		{
		$row[0] = substr($row[0], 0, 4);
		}
	return($row[0]);
	}

########|###############################################################
sub					partition_value
#
# [0] value as date (2019-06-03)
# [1] value as days (737578)
########|###############################################################
	{
	my ($partition) = @_;

	if	($partition->[1] == 0)
		{
		return('0');
		}
	elsif	($partition->[1] == MAXVALUE)
		{
		return('maxvalue');
		}

	return('to_days(' . dq . $partition->[0] . dq . ')');
	}

########|###############################################################
sub					roll_up_days
#
# Note that we only do 1 month and then quit because the months may be
# discontinuous and MySQL will reject the attempt to reorganize
# partitions if there are any gaps.
#
# [0] value as date (2019-06-03)
# [3] days from today (1)
########|###############################################################
	{
	my ($table, $days_before_rollup) = @_;
	my @day_partitions;
	my @month_partitions;
	my @old_partitions;

# get the partitions to roll up and reorganize
# [3] days from today (1)
	foreach	(@current_partitions)
		{
		next unless @$_[3];
		if	(@$_[3] < $days_before_rollup)
			{
			last;
			}
		push @day_partitions, $_;
		if	(@$_[0] =~ '-01$')
			{
#$DB::single = 1;
			if	(scalar(@day_partitions) == 1)
				{
				pop @day_partitions;
				next;
				}
			else
				{
				push @old_partitions, @day_partitions;
				@day_partitions = ();
				}
			push @month_partitions, $_;
			last;
			}
		}
#	say Dumper(@roll_up_partitions);

# reorganize the partitions as needed, creating as needed
	if	(scalar(@old_partitions))
		{
		print tab
			. '${DATE} +'
			. sq
			. '%F %T Roll up day partitions into month partitions'
			. sq
			. nl
			;
		alter_table($table, \@old_partitions, \@month_partitions);
		}
	else
		{
		print tab
			. '${DATE} +'
			. sq
			. '%F %T No day partitions need to be rolled up.'
			. sq
			. nl
			;
		}
	print nl;

	return;
	}

########|###############################################################
sub					roll_up_months
#
# Note that we only do 1 year and then quit because the years may be
# discontinuous and MySQL will reject the attempt to reorganize
# partitions if there are any gaps.
#
# [0] value as date (2019-06-03)
# [3] days from today (1)
########|###############################################################
	{
	my ($table, $months_before_rollup) = @_;
	my @month_partitions;
	my @year_partitions;
	my @old_partitions;
#$DB::single = 1;

# get the partitions to roll up and reorganize
# [3] days from today (1)
	foreach	(@current_partitions)
		{
		next unless @$_[3];
		if	((@$_[3] > ($months_before_rollup * 30))
		&&	 (@$_[0] =~ '-01$'))
			{
			push @month_partitions, $_;
			if	(@$_[0] =~ '-01-01$')
				{
#$DB::single = 1;
				if	(scalar(@month_partitions) == 1)
					{
					pop @month_partitions;
					next;
					}
				else
					{
					push @old_partitions, @month_partitions;
					@month_partitions = ();
					}
				push @year_partitions, $_;
				last;
				}
			}
		}
#	say Dumper(@roll_up_partitions);
#	say Dumper(@reorganize_partitions);

# reorganize the partitions as needed, creating as needed
	if	(scalar(@old_partitions))
		{
		print tab
			. '${DATE} +'
			. sq
			. '%F %T Roll up month partitions into year partitions'
			. sq
			. nl
			;
		alter_table($table, \@old_partitions, \@year_partitions);
		}
	else
		{
		print tab
			. '${DATE} +'
			. sq
			. '%F %T No month partitions need to be rolled up.'
			. sq
			. nl
			;
		}
	print nl;

	}

########|###############################################################
# main
########|###############################################################

# define our command line options
GetOptions(
# help
	  'help'			=> \my $help
	, 'full-help'			=> \my $full_help
	, 'future-help'			=> \my $future_help

	, 'database=s'			=> \my $database
	, 'debug'			=> \$debug
	, 'disable-binlog'		=> \$disable_binlog
	, 'host=s'			=> \my $host
	, 'login-path=s'		=> \my $login_path
	, 'password=s'			=> \my $password
	, 'table=s'			=> \my $table
	, 'user=s'			=> \my $user
	, 'days=n'			=> \my $days_before_rollup
	, 'months=n'			=> \my $months_before_rollup

# full-help
	, 'analyze'			=> \my $analyze
	, 'examples'			=> \my $examples
	, 'stop-on-error'		=> \my $stop_on_error
	, 'verbose'			=> \my $verbose

# future-help

# just for me
	, 'test'			=> \my $test
	);

# need examples?
	if	($examples)
		{
		examples;
		print nl;
		exit(0);
		}

# need help?
	if	($help)
		{
		help;
		print nl;
		exit(0);
		}

# need more help?
	if	($full_help)
		{
		help;
		full_help;
		print nl;
		exit(0);
		}

# dreaming of more help?
	if	($future_help)
		{
		future_help;
		print nl;
		exit(0);
		}

# defaults

# just for me
	if	($test)
		{
		$analyze = 0;
		$database = 'xxxx'				unless $database;
		$days_before_rollup = 5				unless $days_before_rollup;
		$host = 'db10'					unless $host;
		$months_before_rollup = 2			unless $months_before_rollup;
		$password = '6e59522ea7c8bfb3bb970e4b12691510'	unless $password;
		$table = 'tpvs'					unless $table;
		$user = 'manage-partitions'			unless $user;
		}

# handle meta options

# sanity checks
	my $errors = '';
	if	(!$login_path)
		{
		$errors .= 'Please specify --host.' . nl unless $host;
		$errors .= 'Please specify --user.' . nl unless $user;
		}
	$errors .= 'Please specify --database.' . nl unless $database;
	$errors .= 'Please specify --table.' . nl unless $table;
	if	($errors)
		{
		print $errors;
		exit;
		}

# connect to MySQL
	my $dsn = 'DBI:mysql:'
		. 'database=' . $database . ';'
		. 'host=' . $host . ';'
		;	
	$password =~ tr/\'//d;
	$dbh = DBI->connect(
		  $dsn
		, $user
		, $password
		, {
			  PrintError => 0
			, RaiseError => 1
		}
		);

# prevent parallel execution
	$sth = $dbh->prepare('select get_lock(?, 0)');
	$sth->execute('manage-partitions-' . $table);
	@row = $sth->fetchrow_array();
	if	(1 != $row[0])
		{
		print 'Another instance is already modifying '
			. $table
			. '.'
			. nl
			;
		exit(1);
		}

# build the mysql command stub
	$mysql = tab . 'mysql'					. bs . nl;
	$mysql .= tabx2 . '--login-path=' . $login_path		. bs . nl if $login_path;
	$mysql .= tabx2 . '--database=' . $database		. bs . nl if $database;
	$mysql .= tabx2 . '--disable-auto-rehash'		. bs . nl;
	$mysql .= tabx2 . '--host=' . $host			. bs . nl if $host;
	$mysql .= tabx2 . '--init-command='
		. sq . 'set @@session.sql_log_bin=off;' . sq	. bs . nl if $disable_binlog;
	$mysql .= tabx2 . '--password=' . sq . $password . sq	. bs . nl if $password;
	$mysql .= tabx2 . '--user=' . $user			. bs . nl if $user;

# get all the partitions
# @current_partitions
# [0] value as date (2019-06-03)
# [1] value as days (737578)
# [2] name (2019-06-02)
# [3] days from today (1)
$DB::single = 1;
	$sql = 'select'
		. '  from_days(partition_description) as value_as_date'
		. ', case partition_description'
			. ' when '
			. sq
			. 'MAXVALUE'
			. sq
			. ' then '
			. MAXVALUE
			. ' else partition_description end as value_as_days'
		. ', partition_name as name'
		. ', datediff(current_date, from_days(partition_description)) as days_from_today'
		. ' from information_schema.partitions'
		. ' where table_name = ?'
		. ' and table_schema = ?'
		. ' order by partition_ordinal_position'
		. ';'
		;
	print '# get all the partitions'
		. nl
		. '# '
		. $sql
		. nl
			if $debug;
	$sth = $dbh->prepare($sql);
	$sth->execute($table, $database);
	if	($sth->rows < 2)
		{
# we only work on tables with existing partitions
# (maybe in the future...)
# (would need to add --column)
		print tab
			. 'date +'
			. sq
			. '%F %T The table '
			. bt . $table . bt
			. ' does not have enough partitions.'
			. sq
			. nl
			. tab
			. 'date +'
			. sq
			. '%F %T Please create at least '
			. bt . '0000' . bt
			. ' and '
			. bt . '9999' . bt
			. ' partitions.'
			. sq
			. nl
			;
		exit;
		}
	@current_partitions = @{$sth->fetchall_arrayref()};

# start the script
	print '#!/bin/bash'
		. nl
		. '# Created by '
		. getpwuid($<)
		. ' using '
		. __FILE__
		. ' on '
		. strftime('%Y-%m-%d %H:%M:%S', localtime)
		. nlx2
		;

# show the full command
	print '# The command used to create this script:'
		. nl
		. '#'
		. tab
		. $0
		. '\\'
		. nl
		;
	foreach	(sort @options)
		{
		print '#'
			. tabx2
			. $_
			. "\\"
			. nl
			;
		}
	print '#'
		. tabx2
		. '${end_of_list}'
		. nlx2
		;

# stop on error
	if	($stop_on_error)
		{
		print tab . "set -e" . nl;
		}

# reduce output in non-terminal environments
	print '# Reduce output in non-terminal environments'
		. nl
		. tab . 'if' . tab . '[ -t 1 ]' . nl
		. tabx2 . 'then' . nl
		. tabx2 . 'DATE=date' . nl
		. tab . 'else' . nl
		. tabx2 . 'DATE=:' . nl
		. tabx2 . 'fi' . nlx2
		;

# show when we started
	print tab
		. '${DATE} +'
		. sq
		. '%F %T Starting'
		. sq
		. nlx2
		;

# need a minvalue partition?
#$DB::single = 1;
	if	($current_partitions[0]->[1] != 0)
		{
		print tab
			. '${DATE} +'
			. sq
			. '%F %T Add a minvalue partition'
		    	. sq
			. nl
			;
		my @reorganize_partitions = ($current_partitions[0]);
		push my @create_partitions, ['0000-00-00', 0, '0000-00-00'];
		push @create_partitions, $current_partitions[0];
		alter_table($table, \@reorganize_partitions, \@create_partitions);
		print nl;
		}

# need a maxvalue partition?
	if	($current_partitions[$#current_partitions]->[1] != MAXVALUE)
		{
		print tab
			. '${DATE} +'
			. sq
			. '%F %T Add a maxvalue partition'
		    	. sq
			. nl
			. $mysql
			. tabx2
			. '--execute='
			. sq
			. bs
			. nl
			. tabx3
			. 'alter table '
			. bt . $table . bt
			. bs
			. nl
			. tabx4
			. 'add partition (partition '
			. bt . '9999' . bt
			. ' values less than (maxvalue))'
			. bs
			. nl
			. tabx4
			. ';'
			. sq
			. bs
			. nl
			. tabx2				
			. '2>&1'
			. ' | awk '
			. sq
			. '$13 != "insecure."'
			. sq
			. nlx2
			;
		}

# create the partitions we want
	create_partitions($table, $days_before_rollup);

# roll up days into months (last day of month)
	roll_up_days($table, $days_before_rollup);

# roll up months into years (last day of December)
	roll_up_months($table, $months_before_rollup);

# analyze partitions
	if	(($analyze)
	and	 (@analyze_partitions))
		{
		print tab
			. '${DATE} +'
			. sq
			. '%F %T Analyze the partitions we created'
			. sq
			. nl
			;
		foreach	(@analyze_partitions)
			{
			print $mysql
				. tabx2
				. '--batch'
				. bs
				. nl
				. tabx2
				. '--skip-column-names'
				. bs
				. nl
				. tabx2
				. '--skip-table'
				. bs
				. nl
				. tabx2			    
				. '--execute='
				. sq
				. bs
				. nl
				. tabx3
				. 'alter table '
				. bt . $table . bt
				. nl
				. tabx4
				. 'analyze partition'
				. ' '
				. bt . partition_name($_) . bt
				. nl
				. tabx4
			    	. ';'
				. sq
				. bs
				. nl
				. tabx2
				. '2>&1'
				. ' | awk '
				. sq
				. '$4 != "OK" && $13 != "insecure."'
				. sq
				. nl
				;
			}
		print nl;
       		}

# output an end of script marker
	print tab
		. '${DATE} +'
		. sq
		. '%F %T Finished'
		. sq
		. nlx2
		. '# (end of script)'
		. nl
		;

# disconnect from the MySQL database
#	$dbh->disconnect();

# (end of manage-partitions.pl)
