//
//	Filename:	branch.c
//
#define	Version		"031"
//
//	Edit date:	2022-11-26
//
//	Facility:	Asterisk
//
//	Abstract:	This AGI 'script' implements the BRANCH widget.
//
//	Environment:	Asterisk
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	2009-05-22	SLE	Create.
//	001	2009-07-20	SLE	Fix setting the channel
//					  variable to the incorrect
//					  value if an invalid entry is
//					  made.
//	002	2010-06-04	SLE	Trim openlog ident using
//					  basename.
//					Fix bug allowing invalid entry
//					  to be set as the value of
//					  the channel variable.
//					Fix bug using the branch
//					  target list as the channel
//					  variable name.
//	003	2010-11-10	SLE	Use basename() in usage.
//	004	2010-11-11	SLE	Record prompt.
//	005	2010-12-02	SLE	Implement playback list.
//	006	2010-12-22	SLE	Use play_prompt_list().
//	007	2011-01-06	SLE	Add atexit/cleanup.
//					Move COMPLIANT code to the
//					  dialplan.
//					Remove LANGUAGE.
//	008	2011-03-06	SLE	Add flags.
//	009	2012-06-24	SLE	Make mode variables global.
//	010	2012-09-12	SLE	Change IDX to 3 digits.
//					Allow labels in the target
//					  list.
//	011	2012-11-19	SLE	Add input_variable_name.
//					Change the branch target
//					  separator to comma.
//	012	2013-04-29	SLE	Redo DATA parsing.
//					Add response-list.
//					Remove STEP-${IDX}-RESPONSE.
//	013	2014-09-03	SLE	Add DOUBLE_DIGIT_BRANCH.
//	014	2015-03-12	SLE	Add NEXT label.
//	015	2017-12-09	SLE	Change prompt to 1024.
//					Change retry_prompt to 1024.
//	016	2018-01-09	SLE	Add SAME label.
//	017	2018-05-31	SLE	Handle status == -1.
//	018	2019-06-19	SLE	Add RECORDING-IDX.
//	019	2020-01-16	SLE	Homogenize and update.
//	020	2020-01-19	SLE	Add INTERRUPTABLE.
//	021	2020-03-20	SLE	If DOUBLE_DIGIT_BRANCH is set,
//					  return the output variable
//					  as 2 digits, zero filled.
//	022	2020-06-05	SLE	Don't syslog() in hangup().
//	023	2021-01-27	SLE	Add start_time.
//					Add syslog_prefix.
//	024	2021-02-01	SLE	Change call_id to int.
//					Change call_id to global.
//					Change client_id to global.
//	025	2021-02-08	SLE	Use agi_stopmonitor().
//	026	2021-03-23	SLE	Treat *IDX as an integer.
//	027	2021-04-19	SLE	Use STEP-PREFIX.
//	028	2022-01-25	SLE	Raise the syslog level for the
//					  duration message.
//					Log each keystroke.
//	029	2022-09-05	SLE	Add set_tag().
//	030	2022-10-21	SLE	Rename stt_text to
//					  computer_response.
//					Add CUSTOMER-RESPONSE.
//	031	2022-11-26	SLE	Add resolve_variables().

////////////////////////////////////////|//////////////////////////////////////
// Usage
////////////////////////////////////////|//////////////////////////////////////
//
// Channel variables read:
//
//	RECORDING-PATH - path to record files
//
//	STEP-IDX-DATA -
//
//		[FOO=]10,20,30[^BAR][#RESPONSE-1,RESPONSE-2,RESPONSE-2]
//
//	Where:
//		FOO is the optional output variable.
//
//		1,2,3 is the target list (labels or step idx). NEXT is
//		a special target set to the next step in the
//		recipe. SAME is special target set to the current
//		step.
//
//		BAR is the optional input variable.
//
//		RESPONSE-LIST,etc is the optional response list.
//
//	STEP-IDX-FLAGS - DOUBLE_DIGIT_BRANCH if the branch has more
//	than 9 targets. 
//
//	STEP-IDX-PROMPT - path of file to play before DTMF entry.
//
//	STEP-IDX-RETRY-PROMPT - path of file to play before retrying
//	DTMF entry.
//
// Channel variables set:
//
//	IDX - step number to execute next.
//
// Sample database entry:
//
//-- choose option
//	insert into steps set
//		  created		= current_timestamp
//		, data			= 'TYPE-2=GAS,ELEC,BOTH'
//		, idx			= @IDX := @IDX + 1
//		, prompt		= '01-prompt'
//		, recipe		= @RECIPE
//		, type			= 'BRANCH'
//		;
//
// This step would:
//
// 1) Play a prompt asking the caller to enter a digit.
// 2) If the number entered is not a 1, 2, or 3, ask again.
// 3) Set the channel variable TYPE-2 with the number entered.
// 4) Set the channel variable IDX to the step number associated with
// the respective label.

////////////////////////////////////////|//////////////////////////////////////
// ANSI include files
////////////////////////////////////////|//////////////////////////////////////
#include	<ctype.h>
#include	<signal.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

////////////////////////////////////////|//////////////////////////////////////
// Operating system include files
////////////////////////////////////////|//////////////////////////////////////
#include	<syslog.h>

////////////////////////////////////////|//////////////////////////////////////
// Local include files
////////////////////////////////////////|//////////////////////////////////////
#include	"agi.h"
#include	"all.h"
#include	"getopt.h"
#include	"xxxx.h"

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////
global	int				call_id;
global	int				client_id;
global	int				debug_mode;
global	char				syslog_prefix[256];
global	char				syslog_prefix[256];
global	int				test_mode;
global	int				verbose_mode;

////////////////////////////////////////|//////////////////////////////////////
// External variables
////////////////////////////////////////|//////////////////////////////////////
extern	char				*optarg;

////////////////////////////////////////|//////////////////////////////////////
// External functions
////////////////////////////////////////|//////////////////////////////////////
extern	int				resolve_variables
	(
	  char				*string_pointer
	);

extern	int				set_tag
	(
	  char				*step_prefix
	, int				recording_idx
	);

////////////////////////////////////////|//////////////////////////////////////
// Static variables
////////////////////////////////////////|//////////////////////////////////////
static	time_t				start_time;

////////////////////////////////////////|//////////////////////////////////////
// Static functions
////////////////////////////////////////|//////////////////////////////////////
static	void				cleanup
	(
	  void
	);
static	void				hangup
	(
	  void
	);

////////////////////////////////////////|//////////////////////////////////////
// Main function
////////////////////////////////////////|//////////////////////////////////////
global	int				main
	(
	  int				argc
	, char				**argv
	)
	{
	auto	int			attempts;
	static	const char		delimiters[] = ", ";
	auto	int			digit_timeout = 10;
	auto	char			data[1024];
	auto	char			escape_digits[256];
	auto	int			flags;
	auto	int			idx;
	auto	unsigned int		index;
	auto	int			input_variable;
	auto	char			input_variable_name[256];
	static	struct option		long_options[]
		= {
		  {"debug-mode", no_argument, &debug_mode, 1}
		, {"help", no_argument, 0, '?'}
		, {"null", no_argument, 0, 0}
		, {"test-mode", no_argument, &test_mode, 1}
		, {"verbose-mode", no_argument, &verbose_mode, 1}
		, {"version", no_argument, 0, 'v'}
		, {0, 0, 0, 0}
		};
	auto	int			optchar;
	auto	char			output_variable_name[256];
	auto	char			*pointer1;
	auto	char			*pointer2;
	auto	char			prompt[1024];
	auto	char			*prompt_pointer;
	auto	int			recording_idx;
	auto	char			recording_path[256];
	auto	char			response_list[1024];
	auto	char			*response[100];
	auto	char			retry_prompt[1024];
	auto	int			status;
	auto	char			step_prefix[256];
	auto	char			target_list[1024];
	auto	char			*target[100];
	auto	char			temp1[256];
	auto	char			temp2[256];
	auto	int			tens_digit;

// save the start time
	start_time = time(0);

// set the syslog ident
	setlogmask(LOG_UPTO(LOG_NOTICE));
	openlog(basename(argv[0]), LOG_PID, LOG_USER);

// trap SIGHUP -- caller hung up
	signal(SIGHUP, (void (*)(int))(long int)hangup);

// read the AGI environment
	agi_read_environment();

// assume failure
	agi_set_status_failure();

// get the CALL-ID
	call_id = agi_get_integer_variable("CALL-ID");

// get the CLIENT-ID
	client_id = agi_get_integer_variable("CLIENT-ID");

// create the syslog prefix
	sprintf(syslog_prefix
		, "%03d-%08d"
		, client_id
		, call_id
		);

// get DIGIT-TIMEOUT
	digit_timeout = agi_get_integer_variable("DIGIT-TIMEOUT");

// get IDX
	idx = agi_get_integer_variable("IDX");

// set the NEXT label
	memset(temp1, 0, sizeof(temp1));
	sprintf(temp1, "%03d", idx + 1);
	agi_set_variable("LABEL-NEXT", temp1);

// get RECORDING-IDX
	recording_idx = agi_get_integer_variable("RECORDING-IDX");

// get RECORDING-PATH
	memset(recording_path, 0, sizeof(recording_path));
	agi_get_variable("RECORDING-PATH", recording_path);

// set the SAME label
	memset(temp1, 0, sizeof(temp1));
	sprintf(temp1, "%03d", idx);
	agi_set_variable("LABEL-SAME", temp1);

// get step prefix
	memset(step_prefix, 0, sizeof(step_prefix));
	agi_get_variable("STEP-PREFIX", step_prefix);

// get STEP-PREFIX-DATA
	memset(data, 0, sizeof(data));
	agi_get_variable("%s-DATA", data, step_prefix);

// get DIGIT-TIMEOUT
	BEGIN_LOCAL_BLOCK
	auto	int			temp;
	temp = agi_get_integer_variable("%s-DIGIT-TIMEOUT", step_prefix);
	if	(0 != temp)
		{
		digit_timeout = temp;
		}
	END_LOCAL_BLOCK

// get STEP-PREFIX-FLAGS
	flags = agi_get_integer_variable("%s-FLAGS", step_prefix);

// get STEP-PREFIX-PROMPT
	memset(prompt, 0, sizeof(prompt));
	agi_get_variable("%s-PROMPT", prompt, step_prefix);

// get STEP-PREFIX-RETRY-PROMPT
	memset(retry_prompt, 0, sizeof(retry_prompt));
	agi_get_variable("%s-RETRY-PROMPT", retry_prompt, step_prefix);
		
// set TAG
	set_tag(step_prefix, recording_idx);

// process the command line arguments
	while	(-1 != (optchar
			= getopt_long(
				  argc
				, argv
				, ""
				, long_options
				, 0
				)))
		{
		switch	(optchar)
			{
// help
			case '?':
			default:
				printf("Usage:\t%s\\\n", basename(argv[0]));
				puts("\t\t--debug-mode");
				puts("\t\t--help\\");
				puts("\t\t--verbose-mode\\");
				puts("\t\t--version");
				exit(EXIT_SUCCESS);
				break;
// version
			case 'v':
				printf("%s version %s\n"
					, *argv
					, Version
					);
				printf("Compiled on %s at %s\n"
					, __DATE__
					, __TIME__
					);
				exit(EXIT_SUCCESS);
				break;

// something automagically handled by getopt_long()
			case 0:
				break;
			}
		}

// set verbose mode
	if	(0 != verbose_mode)
		{
		setlogmask(LOG_UPTO(LOG_INFO));
		}

// set debug mode
	if	(0 != debug_mode)
		{
		setlogmask(LOG_UPTO(LOG_DEBUG));
		}

// say who we are
	syslog(LOG_INFO
		, "%s Starting, version %s"
		, syslog_prefix
		, Version
		);

// make sure we clean up after ourselves
	atexit(&cleanup);

// branches are normally interruptable
	memset(escape_digits, 0, sizeof(escape_digits));
	strcpy(escape_digits, "#*0123456789");

// unless requested as non-interruptable
// and not test mode
	if	((0 == test_mode)
	&&	 (0 != (NON_INTERRUPTABLE & flags)))
		{
		strcpy(escape_digits, "*");
		}
//	syslog(LOG_NOTICE
//		, "%s test_mode = %d, escape_digits = '%s'"
//		, syslog_prefix
//		, test_mode
//		, escape_digits
//		);

// split up DATA
	syslog(LOG_DEBUG
		, "%s data = %s"
		, syslog_prefix
		, data
		);
// [output-variable-name=]target-list[^input-variable-name][#response-list]
	memset(input_variable_name, 0, sizeof(input_variable_name));
	memset(output_variable_name, 0, sizeof(output_variable_name));
	memset(response_list, 0, sizeof(response_list));
	memset(target_list, 0, sizeof(target_list));
	input_variable = 0;
	pointer1 = data;

// output-variable-name
	if	((pointer2 = strchr(pointer1, '=')))
		{
		syslog(LOG_DEBUG
			, "%s output_variable_name = %s"
			, syslog_prefix
			, pointer1
			);
		*pointer2++ = 0;
		strcpy(output_variable_name, pointer1);
		pointer1 = pointer2;
		}
	syslog(LOG_DEBUG
		, "%s output_variable_name = %s"
		, syslog_prefix
		, output_variable_name
		);
	if	('*' == *output_variable_name)
		{
		memmove(output_variable_name, output_variable_name + 1, sizeof(output_variable_name));
		while	(0 != resolve_variables(output_variable_name))
			;
		}

// response-list
	if	((pointer2 = strchr(pointer1, '#')))
		{
		*pointer2++ = 0;
		strcpy(response_list, pointer2);
		}
	syslog(LOG_DEBUG
		, "%s response_list = %s"
		, syslog_prefix
		, response_list
		);

// input-variable-name
	if	((pointer2 = strchr(pointer1, '^')))
		{
		*pointer2++ = 0;
		strcpy(input_variable_name, pointer2);
		if	(0 != isdigit(*input_variable_name))
			{
			input_variable = atoi(input_variable_name);
			}
		else
			{
//			agi_get_variable(input_variable_name, temp1);
//			input_variable = atoi(temp1);
			input_variable = agi_get_integer_variable(input_variable_name);
			}
//		input_variable += '0';
		}
	syslog(LOG_DEBUG
		, "%s input_variable_name = %s"
		, syslog_prefix
		, input_variable_name
		);
	syslog(LOG_DEBUG
		, "%s input_variable value = %d"
		, syslog_prefix
		, input_variable
		);

// target-list
	strcpy(target_list, pointer1);
	syslog(LOG_DEBUG
		, "%s target_list = %s"
		, syslog_prefix
		, target_list
		);
//	agi_get_variable(target_list, target_list);

// parse the target list
	memset(target, 0, sizeof(target));
	if	(0 != (ALLOW_ZERO & flags))
		{
		index = 0;
		}
	else
		{
		index = 1;
		}
	target[index++] = pointer2 = strtok(target_list, delimiters);
//	for	(; index < (sizeof(target) / sizeof(target[0])); ++index)
	for	(; pointer2; ++index)
		{
		if	(0 == (target[index] = pointer2 = strtok(0, delimiters)))
			{
			break;
			}
		}

// resolve any labels in the target list
//	for	(index = 0; index < (sizeof(target) / sizeof(target[0])); ++index)
	if	(0 != (ALLOW_ZERO & flags))
		{
		index = 0;
		}
	else
		{
		index = 1;
		}
	for	(; target[index]; ++index)
		{
		if	((0 != target[index])
		&&	 (0 != isalpha(*target[index])))
			{
			memset(temp1, 0, sizeof(temp1));
			sprintf(temp1, "LABEL-%s", target[index]);
			status = agi_get_variable(temp1, target[index]);
//			status = agi_get_variable("LABEL-%s", target[index], target[index]);
			if	(EXIT_FAILURE == status)
				{
				sprintf(temp2, "T:%s is not a valid label", temp1);
				status = play_prompt_list(
					  temp2
					, 0
					, 0
					, 0
					, 0
					);
				agi_hangup();
				exit(EXIT_FAILURE);
				}
			}
		}

// parse the response list
	memset(response, 0, sizeof(response));
	response[1] = strtok(response_list, delimiters);
	for	(index = 2; index < (sizeof(response) / sizeof(response[0])); ++index)
		{
		if	(0 == (response[index] = strtok(0, delimiters)))
			{
			break;
			}
		}

// until they get it right
	attempts = 0;
	prompt_pointer = prompt;
	for	(;;)
		{
		tens_digit = 0;

// allow 3 attempts
		if	(++attempts > 3)
			{
// set our exit status
			if	(0 == agi_environment.result)
				{
				agi_set_variable("EXIT-STATUS", "TIMEOUT");
				agi_set_variable("STATUS", "TIMEOUT");
				exit(EXIT_SUCCESS);
				}
			exit(EXIT_FAILURE);
			}

// play and record the prompt
		if	(0 != *prompt_pointer)
			{
			status = play_prompt_list(
				  prompt_pointer
				, recording_path
				, escape_digits
				, recording_idx
				, flags
				);
			agi_environment.result = status;
			if	(-1 == status)
				{
				return(EXIT_SUCCESS);
				}
			}

// do we have an input variable
		if	(0 != *input_variable_name)
			{
			index = input_variable;
			if	(0 != (ZERO_BASED_TARGET_LIST & flags))
				{
				index++;
				}
			break;
			}

// change to the retry prompt
		if	(0 != *retry_prompt)
			{
			prompt_pointer = retry_prompt;
			}

// accept the branch number
		if	(0 == status)
			{
			agi_exec("wait for digit %d000"
				, digit_timeout
				);
			if	(0 == agi_environment.result)
				{
				continue;
				}
			syslog(LOG_ERR
				, "%s key = %c"
				, syslog_prefix
				, agi_environment.result
				);
			status = agi_environment.result;
			if	(-1 == status)
				{
				return(EXIT_SUCCESS);
				}
			}

// should we abort?
		if	('*' == status)
			{
			agi_set_variable("STATUS", "*");
			exit(EXIT_SUCCESS);
			}

// did they enter anything?
		if	('#' == status)
			{
			continue;
			}

// 0 is invalid
//		if	('0' == status)
//			{
//			continue;
//			}

// double digit?
		if	(0 != (DOUBLE_DIGIT_BRANCH & flags))
			{
			tens_digit = (status - '0') * 10;
			agi_exec("wait for digit %d000"
				, digit_timeout
				);
			if	(0 == agi_environment.result)
				{
				continue;
				}
			syslog(LOG_ERR
				, "%s key = %c"
				, syslog_prefix
				, agi_environment.result
				);
			status = agi_environment.result;
			if	(-1 == status)
				{
				return(EXIT_SUCCESS);
				}
// should we abort?
			if	('*' == status)
				{
				agi_set_variable("STATUS", "*");
				exit(EXIT_SUCCESS);
				}
// did they enter anything?
			if	('#' == status)
				{
				continue;
				}
			}

// convert the character into an index
		index = tens_digit + (status - '0');

// validate the index
		if	(0 != target[index])
			{
			break;
			}

		}

// set named variable if requested
	if	(0 != *output_variable_name)
		{
		memset(temp2, 0, sizeof(temp2));
		if	(flags & DOUBLE_DIGIT_BRANCH)
			{
			sprintf(temp2, "%02d", index);
			}
		else
			{
			sprintf(temp2, "%d", index);
			}
		agi_set_variable(output_variable_name, temp2);
		}

// set per_step.customer_response
	BEGIN_LOCAL_BLOCK
	auto	char			name[32];
	sprintf(name
		, "%03d-CUSTOMER-RESPONSE"
		, recording_idx
		);
	agi_set_variable(name, temp2);
	END_LOCAL_BLOCK

// set the step index
	sprintf(temp1, "%03d", atoi(target[index]) - 1);
	agi_set_variable("IDX", temp1);

// copy the response if requested
	if	(0 != response[index])
		{
		auto	char	command[256];
		auto	char	*last_slash_pointer;
		auto	char	response_path[256];
		memset(command, 0, sizeof(command));
		memset(response_path, 0, sizeof(response_path));
		last_slash_pointer = strrchr(response[index], '/');
		*last_slash_pointer = 0;
		if	('/' == *response[index])
			{
			sprintf(
				  command
				, "cp %s/%s/%s.wav %s/%03d-response.wav"
				, response[index]
				, agi_environment.agi_language
				, last_slash_pointer + 1
				, recording_path
				, recording_idx
				);
			}
		else
			{
			sprintf(
				  command
				, "cp /var/lib/asterisk/sounds/%s/%s/%s.wav %s/%03d-response.wav"
				, response[index]
				, agi_environment.agi_language
				, last_slash_pointer + 1
				, recording_path
				, recording_idx
				);
			}
//		syslog(LOG_DEBUG
		syslog(LOG_ERR
			, "%s %s"
			, syslog_prefix
			, command
			);
		system(command);
//		agi_exec("stream file \"%s\" \"\""
//			, response[index]
//			);
		}

// set our exit status
	if	(0 == agi_environment.result)
		{
		agi_set_variable("EXIT-STATUS", "TIMEOUT");
		exit(EXIT_SUCCESS);
		}

// return our status
	agi_set_status_success();

// log the duration
	syslog(LOG_NOTICE
		, "%s %d seconds"
		, syslog_prefix
		, (int)(time(0) - start_time)
		);

// function exit
	return(EXIT_SUCCESS);		// return function status
	}				// end of main()

////////////////////////////////////////|///////////////////////////////|//////
// cleanup()
//
////////////////////////////////////////|///////////////////////////////|//////
static	void				cleanup
	(
	  void
	)
	{

// make sure monitoring has stopped
	agi_stopmonitor();

	exit(EXIT_SUCCESS);
	}

////////////////////////////////////////|///////////////////////////////|//////
// hangup()
//
// Asterisk delivers a SIGHUP when the caller hangs up. Since we have
// already registered a process termination function, all we have to
// do is exit.
////////////////////////////////////////|///////////////////////////////|//////
static	void				hangup
	(
	  void
	)
	{
//	syslog(LOG_ERR, "hangup detected");
	exit(EXIT_SUCCESS);
	}

// (end of branch.c)
