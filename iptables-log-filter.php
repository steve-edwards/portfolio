#!/usr/bin/env php
<?php
//
//	Filename:	iptables-log-filter.php
//
//	Version:	002
//
//	Edit date:	2020-04-20
//
//	Facility:	System administration
//
//	Abstract:	This script reformats iptables kernel logs
//			into a consistent format that can be fed into
//			iptables.
//
//			Mostly used to create firewall access rules.
//
//	Environment:	Linux
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	2015-11-18	SLE	Create.
//	001	2017-10-22	SLE	Add ctstate.
//	002	2020-04-20	SLE	Add -r.
//					Add --router-mode.

// Usage:
//
//	grep kernel: /var/log/messages >kernel
//	./iptables-log-filter.php [--router-mode] <kernel\
//		| sort\
//		| uniq --count\
//		| sort --numeric-sort --reverse\
//		>iptables.tmp

// may come in handy some day
// $comp = preg_split('/\s+/', $var);

// declare variables
	$pair = array();

// define variables
	$delimiters = " \n\t";

// get our command line arguments
        $options = getopt('r', array('router-mode'));

// router mode?
// (include interface options and parameters)
	$router_mode = 0;
	if	((array_key_exists('r', $options))
	||	 (array_key_exists('router-mode', $options)))
		{
		$router_mode = 1;
		}

// loop through stdin
	while	($line = fgets(STDIN))
		{

// blank lines and comments pass through unscathed
		if	(('#' == substr($line, 0, 1))
		||	 (PHP_EOL == $line))
			{
			echo $line;
			continue;
			}

// skip cruft
		if	(0 == strpos($line, 'kernel:'))
			{
			continue;
			}
		if	(0 == strpos($line, 'IN='))
			{
			continue;
			}

// debugging
//		printf("line:%d:%s\n", strpos($line, 'IN='), $line);
//		printf("line:%d:%s\n", strlen($line), $line);

// tokenize the line
		unset($COMMIT);
		unset($FORWARD);
		unset($INPUT);
		unset($OUTPUT);
		unset($POSTROUTING);
		unset($PREROUTING);
		unset($append);
		unset($ctstate);
		unset($destination);
		unset($dport);
		unset($icmp_type);
		unset($in_interface);
		unset($jump);
		unset($log_prefix);
		unset($new_chain);
		unset($out_interface);
		unset($protocol);
		unset($source);
		unset($sport);
		unset($state);
		unset($table);
		unset($to_destination);
		unset($to_source);
//		unset($match);
		$token = strtok($line, $delimiters);
		while	($token !== false)
			{
			$pair = explode('=', $token);
			switch	($pair[0])
				{
				case '*filter':
					$table=$pair[0];
					break;
				case '*mangle':
					$table=$pair[0];
					break;
				case '*nat':
					$table=$pair[0];
					break;
				case 'COMMIT':
					$COMMIT = 'COMMIT';
					break;
				case ':FORWARD':
					$FORWARD = $line;
					break;
				case ':INPUT':
					$INPUT = $line;
					break;
				case ':OUTPUT':
					$OUTPUT = $line;
					break;
				case ':PREROUTING':
					$PREROUTING = $line;
					break;
				case ':POSTROUTING':
					$POSTROUTING = $line;
					break;
				case '--append':
					$append = $pair[1];
					break;
				case '--destination':
				case 'DST':
					$destination = $pair[1];
					break;
				case '--dport':
				case 'DPT':
					$dport = $pair[1];
					break;
				case '--icmp-type':
					$icmp_type = $pair[1];
					break;
				case '--in-interface':
				case 'IN':
					$in_interface = $pair[1];
					break;
				case '--jump':
					$jump = $pair[1];
					break;
				case '--log-prefix':
					$log_prefix = $pair[1];
					break;
//				case '--match':
//					$match = $pair[1];
//					break;
				case '--new-chain':
					$new_chain = $pair[1];
					break;
				case '--out-interface':
				case 'OUT':
					$out_interface = $pair[1];
					break;
				case '--protocol':
				case 'PROTO':
					$protocol = strtolower($pair[1]);
					break;
				case '--source':
				case 'SRC':
					$source = $pair[1];
					break;
				case '--sport':
				case 'SPT':
					$sport = $pair[1];
					break;
				case '--ctstate':
					$ctstate = $pair[1];
					break;
				case '--state':
					$state = $pair[1];
					break;
				case '--to-destination':
					$to_destination = $pair[1];
					break;
				case '--to-source':
					$to_source = $pair[1];
					break;
				}
			$token = strtok($delimiters);
			}

// output miscellaneous lines
		if	(isset($COMMIT))
			{
			printf("COMMIT" . PHP_EOL);
			continue;
			}
		if	(isset($FORWARD))
			{
			printf($line);
			continue;
			}
		if	(isset($INPUT))
			{
			printf($line);
			continue;
			}
		if	(isset($OUTPUT))
			{
			printf($line);
			continue;
			}
		if	(isset($PREROUTING))
			{
			printf($line);
			continue;
			}
		if	(isset($POSTROUTING))
			{
			printf($line);
			continue;
			}
		if	(isset($new_chain))
			{
			printf("\t--new-chain=%s\n", $new_chain);
			continue;
			}
		if	(isset($table))
			{
			printf($table . PHP_EOL);
			continue;
			}

// output the rule in the correct order
// target
		if	(isset($jump))
			{
			printf("\t--jump=%s", $jump);
			}
		else
			{
//			printf("\t--jump=DROP");
			printf("\t--jump=ACCEPT");
			}
// chain
		if	(isset($append))
			{
			printf(" --append=%s", $append);
			}
		else
			{
			if	(empty($out_interface))
				{
				printf(" --append=INPUT");
				}
			elseif	(empty($in_interface))
				{
				printf(" --append=OUTPUT");
				}
			else
				{
				printf(" --append=FORWARD");
				}
			}
// ctstate
		if	(isset($ctstate))
			{
			printf(" --match=conntrack --ctstate=%s", $ctstate);
			}
// state
		if	(isset($state))
			{
			printf(" --match=state --state=%s", $state);
			}
// protocol
		if	(isset($protocol))
			{
			if	(($protocol == 'icmp')
			||	 ($protocol == 'tcp')
			||	 ($protocol == 'udp'))
				{
				printf(" --protocol=%s --match=%s", $protocol, $protocol);
				}
			else
				{
				printf(" --protocol=%s", $protocol);
				}
			}
// match
//		if	(isset($match))
//			{
//			printf(" --match=%s", $match);
//			}
// input interface
		if	((isset($in_interface))
		&&	 (!empty($in_interface))
		&&	 ($router_mode))
			{
			printf(" --in-interface=%s", $in_interface);
			}
// output interface
		if	((isset($out_interface))
		&&	 (!empty($out_interface))
		&&	 ($router_mode))
			{
			printf(" --out-interface=%s", $out_interface);
			}
// source IP address
		if	(isset($source))
			{
			printf(" --source=%s", $source);
			}
// destination IP address
		if	(isset($destination))
			{
			printf(" --destination=%s", $destination);
			}
// source port
		if	(isset($sport))
			{
			printf(" --sport=%s", $sport);
			}
// destination port
		if	(isset($dport))
			{
			printf(" --dport=%s", $dport);
			}
// to destination IP address
		if	(isset($to_destination))
			{
			printf(" --to-destination=%s", $to_destination);
			}
// to source IP address
		if	(isset($to_source))
			{
			printf(" --to-source=%s", $to_source);
			}
// ICMP type
		if	(isset($icmp_type))
			{
			printf(" --icmp-type=%s", $icmp_type);
			}

// log prefix
		if	(isset($log_prefix))
			{
			printf(" --log-prefix=%s", $log_prefix);
			}
		echo PHP_EOL;
		}

// (end of iptables-log-filter.php)?>
